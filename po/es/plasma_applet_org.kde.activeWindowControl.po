# Spanish translations for plasma_applet_org.kde.activeWindowControl.po package.
# Copyright (C) 2017 This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Automatically generated, 2017.
# Eloy Cuadra <ecuadra@eloihr.net>, 2017, 2018, 2019.
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_org\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-05-23 00:18+0000\n"
"PO-Revision-Date: 2019-04-15 18:58+0200\n"
"Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.03.80\n"

#: package/contents/config/config.qml:6
msgid "Appearance"
msgstr "Aspecto visual"

#: package/contents/config/config.qml:11
msgid "Behaviour"
msgstr "Comportamiento"

#: package/contents/config/config.qml:16
msgid "Buttons"
msgstr "Botones"

#: package/contents/config/config.qml:21
msgid "Mouse Control"
msgstr "Control del ratón"

#: package/contents/config/config.qml:26
msgid "Application Menu"
msgstr "Menú de la aplicación"

#: package/contents/ui/config/ConfigAppearance.qml:73
msgctxt "Use default font"
msgid "Default"
msgstr "Por omisión"

#: package/contents/ui/config/ConfigAppearance.qml:90
msgid "Plasmoid version: "
msgstr "Versión del plasmoide:"

#: package/contents/ui/config/ConfigAppearance.qml:96
msgid "Width in horizontal panel:"
msgstr "Anchura en el panel horizontal:"

#: package/contents/ui/config/ConfigAppearance.qml:103
msgid "Fill width"
msgstr "Rellenar el ancho"

#: package/contents/ui/config/ConfigAppearance.qml:122
msgid "Fine tuning:"
msgstr "Ajuste fino:"

#: package/contents/ui/config/ConfigAppearance.qml:142
msgid "Hide titlebar for maximized windows (takes effect immediately)"
msgstr ""
"Ocultar la barra de título en las ventanas maximizadas (tiene efecto "
"inmediato)"

#: package/contents/ui/config/ConfigAppearance.qml:158
msgid "Show window title"
msgstr "Mostrar el título de la ventana"

#: package/contents/ui/config/ConfigAppearance.qml:173
msgid "Text type:"
msgstr "Tipo de texto:"

#: package/contents/ui/config/ConfigAppearance.qml:178
#: package/contents/ui/config/ConfigAppearance.qml:196
#: package/contents/ui/config/ConfigAppearance.qml:201
msgid "Window title"
msgstr "Título de la ventana"

#: package/contents/ui/config/ConfigAppearance.qml:178
#: package/contents/ui/config/ConfigAppearance.qml:196
msgid "Application name"
msgstr "Nombre de la aplicación"

#: package/contents/ui/config/ConfigAppearance.qml:182
msgid "Fit text:"
msgstr "Ajustar texto:"

#: package/contents/ui/config/ConfigAppearance.qml:187
msgid "Just elide"
msgstr "Solo elidir"

#: package/contents/ui/config/ConfigAppearance.qml:187
msgid "Fit on hover"
msgstr "Ajustar al situar el cursor encima"

#: package/contents/ui/config/ConfigAppearance.qml:187
msgid "Always fit"
msgstr "Ajustar siempre"

#: package/contents/ui/config/ConfigAppearance.qml:191
msgid "Tooltip text:"
msgstr "Texto de ayuda emergente:"

#: package/contents/ui/config/ConfigAppearance.qml:196
msgid "No tooltip"
msgstr "Sin texto de ayuda emergente"

#: package/contents/ui/config/ConfigAppearance.qml:233
msgid "No window text:"
msgstr "Sin texto de ventana:"

#: package/contents/ui/config/ConfigAppearance.qml:247
msgid "Use %activity% placeholder to show current activity name."
msgstr ""
"Use el parámetro %activity% para mostrar el nombre de la actividad actual."

#: package/contents/ui/config/ConfigAppearance.qml:254
msgid "No window icon:"
msgstr "Sin icono de ventana:"

#: package/contents/ui/config/ConfigAppearance.qml:265
msgid "Limit text width"
msgstr "Limitar la anchura del texto"

#: package/contents/ui/config/ConfigAppearance.qml:275
#: package/contents/ui/config/ConfigAppMenu.qml:95
msgctxt "Abbreviation for pixels"
msgid "px"
msgstr "px"

#: package/contents/ui/config/ConfigAppearance.qml:288
msgid "Show window icon"
msgstr "Mostrar el icono de la ventana"

#: package/contents/ui/config/ConfigAppearance.qml:294
msgid "Window icon on the right"
msgstr "Icono de la ventana a la derecha"

#: package/contents/ui/config/ConfigAppearance.qml:315
msgid "Bold text"
msgstr "Texto en negrita"

#: package/contents/ui/config/ConfigAppearance.qml:319
msgid "Text font:"
msgstr "Tipo de letra del texto:"

#: package/contents/ui/config/ConfigAppearance.qml:340
msgid "Icon and text spacing:"
msgstr "Espaciado del icono y del texto:"

#: package/contents/ui/config/ConfigAppearance.qml:352
msgid "Font size scale:"
msgstr "Escala del tamaño del tipo de letra:"

#: package/contents/ui/config/ConfigAppMenu.qml:23
msgid "Enable application menu"
msgstr "Activar el menú de la aplicación"

#: package/contents/ui/config/ConfigAppMenu.qml:38
msgid "Fill height"
msgstr "Rellenar la altura"

#: package/contents/ui/config/ConfigAppMenu.qml:44
msgid "Bold font"
msgstr "Tipo de letra en negrita"

#: package/contents/ui/config/ConfigAppMenu.qml:50
#: package/contents/ui/config/ConfigButtons.qml:173
msgid "Do not hide on mouse out"
msgstr "No ocultar cuando salga el ratón"

#: package/contents/ui/config/ConfigAppMenu.qml:55
msgid "Show next to buttons"
msgstr "Mostrar junto a los botones"

#: package/contents/ui/config/ConfigAppMenu.qml:61
msgid "Show next to icon and text"
msgstr "Mostrar junto al icono y el texto"

#: package/contents/ui/config/ConfigAppMenu.qml:67
msgid "Switch sides with icon and text"
msgstr "Cambiar los lados con el icono y el texto"

#: package/contents/ui/config/ConfigAppMenu.qml:74
msgid "Show separator"
msgstr "Mostrar separador"

#: package/contents/ui/config/ConfigAppMenu.qml:81
msgid "Make window title bold when menu is displayed"
msgstr "Poner el título de la ventana en negrita cuando se muestra el menú"

#: package/contents/ui/config/ConfigAppMenu.qml:86
msgid "Side margin:"
msgstr "Margen lateral:"

#: package/contents/ui/config/ConfigAppMenu.qml:99
msgid "Icon and text opacity:"
msgstr "Opacidad del icono y del texto:"

#: package/contents/ui/config/ConfigAppMenu.qml:111
msgid "Menu button text size scale:"
msgstr "Escala del tamaño del texto del botón de menú:"

#: package/contents/ui/config/ConfigBehaviour.qml:14
msgid "Show active window only for plasmoid's screen"
msgstr "Mostrar la ventana activa solo para la pantalla del plasmoide"

#: package/contents/ui/config/ConfigButtons.qml:90
msgid "Enable Control Buttons"
msgstr "Activar botones de control"

#: package/contents/ui/config/ConfigButtons.qml:111
msgid "Show minimize button"
msgstr "Mostrar el botón para minimizar"

#: package/contents/ui/config/ConfigButtons.qml:116
msgid "Show maximize button"
msgstr "Mostrar el botón para maximizar"

#: package/contents/ui/config/ConfigButtons.qml:121
msgid "Show pin to all desktops"
msgstr "Mostrar la chincheta en todos los escritorios"

#: package/contents/ui/config/ConfigButtons.qml:131
msgid "Button order:"
msgstr "Orden de los botones:"

#: package/contents/ui/config/ConfigButtons.qml:167
msgid "Behaviour:"
msgstr "Comportamiento:"

#: package/contents/ui/config/ConfigButtons.qml:184
msgid "Show only when maximized"
msgstr "Mostrar solo cuando esté maximizada"

#: package/contents/ui/config/ConfigButtons.qml:189
msgid "Buttons next to icon and text"
msgstr "Botones junto al icono y al texto"

#: package/contents/ui/config/ConfigButtons.qml:194
msgid "Buttons between icon and text"
msgstr "Botones entre el icono y el texto"

#: package/contents/ui/config/ConfigButtons.qml:200
msgid "Dynamic width"
msgstr "Anchura dinámica"

#: package/contents/ui/config/ConfigButtons.qml:206
msgid "Sliding icon and text"
msgstr "Icono y texto deslizantes"

#: package/contents/ui/config/ConfigButtons.qml:218
msgid "Position:"
msgstr "Posición:"

#: package/contents/ui/config/ConfigButtons.qml:224
msgid "Upper left"
msgstr "Arriba a la izquierda"

#: package/contents/ui/config/ConfigButtons.qml:235
msgid "Upper right"
msgstr "Arriba a la derecha"

#: package/contents/ui/config/ConfigButtons.qml:241
msgid "Bottom left"
msgstr "Abajo a la izquierda"

#: package/contents/ui/config/ConfigButtons.qml:247
msgid "Bottom right"
msgstr "Abajo a la derecha"

#: package/contents/ui/config/ConfigButtons.qml:253
msgid "Vertical center"
msgstr "Centrar verticalmente"

#: package/contents/ui/config/ConfigButtons.qml:263
msgid "Button size:"
msgstr "Tamaño de los botones:"

#: package/contents/ui/config/ConfigButtons.qml:275
msgid "Buttons spacing:"
msgstr "Espaciado de los botones:"

#: package/contents/ui/config/ConfigButtons.qml:294
msgid "Theme"
msgstr "Tema"

#: package/contents/ui/config/ConfigButtons.qml:300
msgid "Automatic"
msgstr "Automático"

#: package/contents/ui/config/ConfigButtons.qml:304
msgid "Absolute path to aurorae button theme folder"
msgstr "Ruta absoluta a la carpeta del tema de botón Aurorae"

#: package/contents/ui/config/ConfigMouseControl.qml:84
msgid "Mouse Buttons:"
msgstr "Botones del ratón:"

#: package/contents/ui/config/ConfigMouseControl.qml:89
msgid "Doubleclick to toggle maximizing"
msgstr "Doble clic para conmutar la maximización"

#: package/contents/ui/config/ConfigMouseControl.qml:101
msgid "Left click disabled"
msgstr "Clic izquierdo desactivado"

#: package/contents/ui/config/ConfigMouseControl.qml:108
msgid "Left click to present windows (Current Desktop)"
msgstr "Clic izquierdo para presentar las ventanas (del escritorio actual)"

#: package/contents/ui/config/ConfigMouseControl.qml:115
msgid "Left click to present windows (All Desktops)"
msgstr "Clic izquierdo para presentar las ventanas (de todos los escritorios)"

#: package/contents/ui/config/ConfigMouseControl.qml:122
msgid "Left click to present windows (Window Class)"
msgstr "Clic izquierdo para presentar las ventanas (de clase ventana)"

#: package/contents/ui/config/ConfigMouseControl.qml:140
msgid "Middle click disabled"
msgstr "Clic central desactivado"

#: package/contents/ui/config/ConfigMouseControl.qml:146
msgid "Middle click to close active window"
msgstr "Clic central para cerrar la ventana activa"

#: package/contents/ui/config/ConfigMouseControl.qml:152
msgid "Middle click to toggle fullscreen"
msgstr "Clic central para conmutar pantalla completa"

#: package/contents/ui/config/ConfigMouseControl.qml:163
msgid "Mouse Wheel:"
msgstr "Rueda del ratón:"

#: package/contents/ui/config/ConfigMouseControl.qml:168
msgid "Mouse wheel up to maximize"
msgstr "Rueda del ratón hacia arriba para maximizar"

#: package/contents/ui/config/ConfigMouseControl.qml:178
msgid "Mouse wheel down disabled"
msgstr "Rueda del ratón hacia abajo desactivada"

#: package/contents/ui/config/ConfigMouseControl.qml:184
msgid "Mouse wheel down to minimize"
msgstr "Rueda del ratón hacia abajo para minimizar"

#: package/contents/ui/config/ConfigMouseControl.qml:190
msgid "Mouse wheel down to unmaximize"
msgstr "Pulsar rueda del ratón para desmaximizar"

#: package/contents/ui/config/IconPicker.qml:73
msgctxt "@item:inmenu Open icon chooser dialog"
msgid "Choose..."
msgstr "Escoger..."

#: package/contents/ui/config/IconPicker.qml:78
msgctxt "@item:inmenu Reset icon to default"
msgid "Clear Icon"
msgstr "Borrar icono"

#: package/contents/ui/main.qml:583
msgid "Switch to activity: %1"
msgstr "Cambiar a la actividad: %1"

#: package/contents/ui/main.qml:591
msgid "Close"
msgstr "Cerrar"

#: package/contents/ui/main.qml:592
msgid "Toggle Maximise"
msgstr "Conmutar «Maximizar»"

#: package/contents/ui/main.qml:593
msgid "Minimise"
msgstr "Minimizar"

#: package/contents/ui/main.qml:594
msgid "Toggle Pin To All Desktops"
msgstr "Conmutar «Fijar en todos los escritorios»"

#: package/contents/ui/main.qml:596
msgid "Reload Theme"
msgstr "Volver a cargar el tema"
